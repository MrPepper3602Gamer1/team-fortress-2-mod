package mrpepper3602gamer1.mods.teamfortress2mod;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLLoadEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = "TF2Mod" , name = "Team Fortress 2" , version = "Version 0.1 Beta")
@NetworkMod(clientSideRequired=true, serverSideRequired=false)
public class TeamFortress2  {
   
	public static CreativeTabs tabTF2 = new CreativeTabs("tabTF2") {
        public ItemStack getIconItemStack() {
                return new ItemStack(Item.swordDiamond, 1, 0);
        }
};
	

@Instance("TeamFortress2")
public static TeamFortress2 instance;
	
	@EventHandler
	public void Init (FMLInitializationEvent event) {
		
		LanguageRegistry.instance().addStringLocalization("itemGroup.tabTF2", "en_US", "Team Fortress 2");
	}	
	
	@EventHandler
	public void preInit (FMLPreInitializationEvent event) {
	
	}
	
	@EventHandler
	public void load (FMLLoadEvent event) {
			
	}
	
	@EventHandler
	public void postInit (FMLPostInitializationEvent event){
		
	}
	
	@EventHandler
	public void ServerStart (FMLServerStartingEvent event){
	
	}


} 	
	
